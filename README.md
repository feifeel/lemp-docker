# lemp

### Stacks
Nginx

PHP

MariaDB

Mkdocs

### Self signed certificate
Do not forget to generate a new one
```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout server.key -out server.crt
```


